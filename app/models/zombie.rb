
class Zombie < ApplicationRecord
	has_many :brains
	mount_uploader :avatar, AvatarUploader

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

	

	validates :name, presence: true
	validates :bio, length: {in: 2..100, :message => "Biografía del zombie"}
	validates :age, numericality: {only_integer: true, :message => "Solo enteros"}
	validates :email, format: {:with => VALID_EMAIL_REGEX, message: "Formato de correo inválido"}
end
